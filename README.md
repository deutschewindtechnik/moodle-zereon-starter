# Zereon Moodle Starter

Dieses Repository dient dazu, eine neue Moodle Zereon Installation zu kopieren. Dafür wird eine Unix Konsole (z.B. git bash) benötigt.
 
```bash
$ git clone git@bitbucket.org:deutschewindtechnik/moodle-zereon-starter.git moodle
$ cd moodle
$ ./git.sh
```